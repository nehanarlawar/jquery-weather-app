$(window).on('hashchange', function(e) {
    var hash = window.location.hash;
    if (hash == "#city/sanjose") {
        var localstorage=get_localstorage_name("San Jose");
		populate_city("San Jose",localstorage);
    }
   if (hash == "#city/sydney") {
	    var localstorage=get_localstorage_name("Sydney");
		populate_city("Sydney",localstorage);
       
    }
   if (hash == "#city/currentlocation") {
	   var localstorage=get_localstorage_name("Current Location");
		populate_city("Current Location",localstorage);
       
    }
   if (hash == "#" || $.isEmptyObject(hash)) {
        if (!$.isEmptyObject($("body").attr("class"))) {
            $("body").removeAttr('class');
        }
		populate_weather_data();
		$('#city_weather').addClass('hide');
        $('#city_list').removeClass('hide');
        $('#city_list').addClass('cities-list');
        $("#nav_menu").removeAttr("class");
        $('#nav_menu').addClass('city-list');
        $('#next_prev').addClass('hide');
        $('#poweredBy').removeClass('hide');
        $('#list_menu').addClass('hide');
    }
});
function update_city_data()
{
	var hash = window.location.hash;
        if (hash == "#city/sanjose")
		{
        var localstorage=get_localstorage_name("San Jose");
		populate_city("San Jose",localstorage);
		}
        else if (hash == "#city/sydney"){
        var localstorage=get_localstorage_name("Sydney");
		populate_city("Sydney",localstorage);
		}
        else if (hash == "#" || $.isEmptyObject(hash)){
            fetch_city_list();
		}
        else if (hash == "#city/currentlocation"){
        var localstorage=get_localstorage_name("Current Location");
		populate_city("Current Location",localstorage);
		}
}
$(document).ready(function() {

    getLocation();
    var temp_format = {
        temp_C_F: "Fahrenheit"
    };
    localStorage.setItem('Temperature_format', JSON.stringify(temp_format));
    $('#degree_Celsius').addClass('not_selected_C_F');
	

	
$('a.degree_C').click(function(e) {
    $('#degree_Fahrenheit').addClass('not_selected_C_F');
    $('#degree_Celsius').removeClass('not_selected_C_F');
        e.preventDefault();
        var temp_format = {
            temp_C_F: "Celsius"
        };
        localStorage.setItem('Temperature_format', JSON.stringify(temp_format));
        update_city_data();
		
    });
$('a.degree_F').click(function(e) {
        $('#degree_Fahrenheit').removeClass('not_selected_C_F');
        $('#degree_Celsius').addClass('not_selected_C_F');
        e.preventDefault();
        var temp_format = {
            temp_C_F: "Fahrenheit"
        };
        localStorage.setItem('Temperature_format', JSON.stringify(temp_format));
        update_city_data();
    });

	setInterval(update_time, 60000);
    setInterval(refresh_data, 600000);
});

function get_localstorage_name(city_name)
{
	var city_list=JSON.parse(localStorage.getItem('CityList_To_Show'));
        for(i=0;i<city_list.length;i++)
			{
				 if(city_name==city_list[i].city_name)
					 return city_list[i].localstoragename;
			}	 
}
function populate_weather_data()
{
	var i;
	var city_list=JSON.parse(localStorage.getItem('CityList_To_Show'));
        for(i=0;i<city_list.length;i++)
			{
		    	fetch_data_fr_countries(city_list[i].latitude,city_list[i].longitude,city_list[i].localstoragename);
			}
		
}
function prepare_citylist()
{
	var cities = JSON.parse(localStorage.getItem('CityList_To_Show'));
	if(!cities)
   {
		var city_list = [
			{
				"city_name"        : "San Jose",
				"longitude"        : "-121.88632860000001",
				"latitude"         : "37.3382082",
				"localstoragename" : "sanjose_data"
				
			},
			{
				"city_name"        : "Sydney",
				"longitude"        : "151.20699020000006",
				"latitude"         : "-33.8674869",
                "localstoragename" : "sydney_data" 				
			},
	];
	
	localStorage.setItem('CityList_To_Show', JSON.stringify(city_list));
  }
}
function add_city(longitude,latitude,city_name)
{
	
	var city_exists=0;
	var cities=JSON.parse(localStorage.getItem('CityList_To_Show'));
	for(i=0;i<cities.length;i++)
	{
		if(cities[i].city_name==city_name)
		{
			city_exists=true;
		}
	}
	if(!city_exists)
	{
		var new_city ={}
		new_city["city_name"]= city_name;
		new_city["longitude"]=longitude;
		new_city["latitude"]=latitude;
		new_city["localstoragename"]=city_name+"_data";
		cities.push(new_city);
		localStorage.setItem('CityList_To_Show', JSON.stringify(cities));
	}
	
}
function showLocation(pos) {
   
    var current_location = {
        longitude: pos.coords.longitude,
        latitude: pos.coords.latitude
    };
	prepare_citylist();
	add_city(pos.coords.longitude,pos.coords.latitude,"Current Location");
    localStorage.setItem('current_location_data', JSON.stringify(current_location));
	populate_weather_data();
}

function showError(error) {
    alert("Error in geolocation :- "+error.message);
}

function getLocation() {
    if (navigator.geolocation) {
        var options = {
            timeout: 5000,
            maximumAge: 0
        };
        navigator.geolocation.getCurrentPosition(showLocation, showError, options);
    } else {
        showError();
    }
};

function refresh_data() {
   populate_weather_data();
}

function update_time() {
    var sanjose_time = $("#time_sanjose").html();
	
	if(!sanjose_time){
		var city_sanjose = JSON.parse(localStorage.getItem('sanjose_data'));
		sanjose_time=format_time(getLocalDate(city_sanjose.currently.time, city_sanjose.offset));
		
	}
    var sydney_time = $("#time_sydney").html();
	if(!sydney_time){
	var city_sydney = JSON.parse(localStorage.getItem('sydney_data'));
		sydney_time=format_time(getLocalDate(city_sydney.currently.time, city_sydney.offset));
	}
    var current_time = $('#time_current').html();
	if(!current_time){
	var city_current = JSON.parse(localStorage.getItem('Current Location_data'));
		current_time=format_time(getLocalDate(city_current.currently.time, city_current.offset));

	}
    sanjose_time = set_time(sanjose_time);
	
    sydney_time = set_time(sydney_time);
	
    current_time = set_time(current_time);
	
    $("#time_sanjose").html(sanjose_time);
    $("#time_sydney").html(sydney_time);
    $('#time_current').html(current_time);
}

function set_time(time_data) {
    var time_arr = time_data.split(" ");
    var time_min = time_arr[0].split(":");
    var minutes = parseInt(time_min[1]);
    var hours = parseInt(time_min[0]);
    if (minutes == 59) {
        minutes = 00;
        if (hours != 12)
            hours++;
        else if (hours == 12)
            hours = 1;
    } else {
        minutes++;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    return hours.toString() + ":" + minutes.toString() + " " + time_arr[1];
}

function fetch_data_fr_countries(latitude,longitude,localstoragename) {
  $.ajax({
        url: "https://api.forecast.io/forecast/c5f012106ffb01a1a3271b297f0917ec/" + latitude + "," + longitude + "?units=us",
        dataType: "jsonp",
        success: function(data) {
            console.log(data);
            localStorage.setItem(localstoragename, JSON.stringify(data));
            var data_time = {
                timeStamp: new Date().getTime()
            };
            localStorage.setItem('Last_upd_time', JSON.stringify(data_time));
            update_city_data();
			
			
        }
    });
	
    
}
function populate_city(city_name,localstoragename)
{
	if(city_name=="San Jose")
	{
		$('#city_weather').removeClass('hide');
		$('#city_list').removeClass('cities-list');
		$('#city_list').addClass('hide');
		$('#next_prev').removeClass('hide');
		$('#poweredBy').addClass('hide');
		$('#list_menu').removeClass('hide');
		$('#prev_nav_link').attr('href',"#city/currentlocation");
		$('#next_nav_link').attr('href',"#city/sydney");
		$('#second_dot').addClass('current');
		$('#first_dot').removeClass('current');
		$('#third_dot').removeClass('current');
		$('#next_nav_link').removeClass('disabled');
		$('#prev_nav_link').removeClass('disabled');
	}
	else if(city_name=="Sydney")
	{
		$('#city_weather').removeClass('hide');
		$('#city_list').removeClass('cities-list');
		$('#city_list').addClass('hide');
		$('#next_prev').removeClass('hide');
		$('#poweredBy').addClass('hide');
		$('#list_menu').removeClass('hide');
		$('#prev_nav_link').attr('href',"#city/sanjose");
		$('#next_nav_link').addClass('disabled');
		$('#second_dot').removeClass('current');
		$('#third_dot').addClass('current');
		$('first_dot').removeClass('current');
		$('#next_nav_link').addClass('disabled');
		$('#prev_nav_link').removeClass('disabled');
		$('#next_nav_link').attr('href',"#city/sydney");
	}
	else if(city_name=="Current Location")
	{
		$('#city_weather').removeClass('hide');
		$('#city_list').removeClass('cities-list');
		$('#city_list').addClass('hide');
		$('#next_prev').removeClass('hide');
		$('#poweredBy').addClass('hide');
		$('#list_menu').removeClass('hide');
		$('#next_nav_link').attr('href',"#city/sanjose");
		$('#prev_nav_link').addClass('disabled');
		$('#second_dot').removeClass('current');
		$('#third_dot').removeClass('current');
		$('#first_dot').addClass('current');
		$('#next_nav_link').removeClass('disabled');
		$('#prev_nav_link').attr('href',"#city/currentlocation");
	}
	
	var temp_formatter = JSON.parse(localStorage.getItem('Temperature_format'));
	var data = JSON.parse(localStorage.getItem(localstoragename));
    var class_name = check_daynight(data);
    $("body").removeAttr("class");
    $("body").addClass(class_name);
    $("#nav_menu").removeAttr("class");
    $("#nav_menu").addClass(class_name);
    $(".city-header").html('');
    $(".today-summary").html('');
    $(".today-details-list").html('');
    var current_data = [
        '<h2>',city_name,'</h2>',
        '<h4>', data.currently.summary, '</h4>',
        '<h1>', format_temp(data.currently.temperature, temp_formatter.temp_C_F), '˚</h1>'
    ].join('');
    $(".city-header").append(current_data);
    $(".today-overview").html('');
    var li_today = [
        '<li>', getWeekDay(getLocalDate(data.currently.time, data.offset)), '</li>',
        '<li>Today</li>',
        '<li>', format_temp(data.daily.data[0].temperatureMax, temp_formatter.temp_C_F), '˚</li>',
        '<li>', format_temp(data.daily.data[0].temperatureMin, temp_formatter.temp_C_F), '˚</li>'
    ].join('');
    $(".today-overview").append(li_today);
    $(".forecast-list").html('');
    for (i = 0; i < data.daily.data.length; i++) {
        var li_list = [
            '<li>',
            '<ul>',
            '<li>', getWeekDay(getLocalDate(data.daily.data[i].time, data.offset)), '</li>',
            '<li><img src="images/', data.daily.data[i].icon, '.png"/></li>',
            '<li>', format_temp(data.daily.data[i].temperatureMax, temp_formatter.temp_C_F), '˚</li>',
            '<li>', format_temp(data.daily.data[i].temperatureMin, temp_formatter.temp_C_F), '˚</li>',
            '</ul>',
            '</li>'
        ].join('');
        $(".forecast-list").append(li_list);
    }
    $(".today-summary").html("Today: "+data.daily.summary);
    $(".today-details-list").append(get_today_details(data));
    get_hour_data(data);

	
}
function fetch_city_list() {
	var data_sanjose = JSON.parse(localStorage.getItem('sanjose_data'));
    var temp_format = JSON.parse(localStorage.getItem('Temperature_format'));
    var display_time=$('#time_sanjose').html();   
    $("#main_info_sanjose").html('');
   var time=  format_time(getLocalDate(data_sanjose.currently.time, data_sanjose.offset));
	var li = [
	    '<li>',
        '<div id="time_sanjose" class="time">',time,'</div>',
        '<div class="city-name">San Jose</div>',
        '</li>',
        '<li id="curr_temp">', format_temp(data_sanjose.currently.temperature, temp_format.temp_C_F), '˚</li>'
    ].join('');
 
    $("#main_info_sanjose").append(li);
    var class_name = check_daynight(data_sanjose);
    $("#city_sanjose").addClass(class_name);
    $("#sydney").html('');
    var data_sydney = JSON.parse(localStorage.getItem('sydney_data'));
    var class_name = check_daynight(data_sydney);
    $("#city_sydney").addClass(class_name);
	 time=  format_time(getLocalDate(data_sydney.currently.time, data_sydney.offset));
    var li_sydney = [
        '<li>',
        '<div id="time_sydney" class="time">',time,'</div>',
        '<div class="city-name">Sydney</div>',
        '</li>',
        '<li>', format_temp(data_sydney.currently.temperature, temp_format.temp_C_F), '˚</li>'
    ].join('');
    $("#sydney").append(li_sydney);
    $("#currentlocation").html('');
    var data_current = JSON.parse(localStorage.getItem('Current Location_data'));
    var class_name = check_daynight(data_current);
    $("#city_current").addClass(class_name);
	 time=  format_time(getLocalDate(data_current.currently.time, data_current.offset));
    var li_current = [
        '<li>',
        '<div id="time_current" class="time">',time,'</div>',
        '<div class="city-name">Current Location</div>',
        '</li>',
        '<li>', format_temp(data_current.currently.temperature, temp_format.temp_C_F), '˚</li>'
    ].join('');
    $("#currentlocation").append(li_current);
}

function getLocalDate(time, offset) {
    var date = new Date(time * 1000);
    var utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes());
    utc.setHours(utc.getHours() + offset);
	return utc;
}

function format_time(utc) {
    var formatted_time;
    var timeOfDay;
    var currentHours = utc.getHours();
    var currentMinutes = utc.getMinutes();
    var timeOfDay = (currentHours < 12) ? "AM" : "PM";
    currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;
    currentHours = (currentHours == 0) ? 12 : currentHours;
    currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
    formatter_date = currentHours + ":" + currentMinutes + " " + timeOfDay;
    return formatter_date;
}

function get_hour_data(data) {
    var temp_format = JSON.parse(localStorage.getItem('Temperature_format'));
    var i = 0;
    $(".today-hourly-list").html('');
    var li_hour = [
        '<li><ul><li>Now</li>',
        '<li><img src="images/', data.hourly.data[0].icon, '.png"/></li>',
        '<li>', format_temp(data.hourly.data[0].temperature, temp_format.temp_C_F), '˚</li></ul></li>'
    ].join('');
    $(".today-hourly-list").append(li_hour);
    for (i = 1; i < 24; i++) {
        var li_hour = [
            '<li><ul><li>', getHours_format(getLocalDate(data.hourly.data[i].time, data.offset)), '</li>',
            '<li><img src="images/', data.hourly.data[i].icon, '.png"/></li>',
            '<li>', format_temp(data.hourly.data[i].temperature, temp_format.temp_C_F), '˚</li></ul></li>'
        ].join('');
        $(".today-hourly-list").append(li_hour);
    }
}

function get_today_details(data) {
    var temp_format = JSON.parse(localStorage.getItem('Temperature_format'));
    var li_today_details_list = [
        '<li><ul> <li>Sunrise:</li>',
        '<li>', format_time(getLocalDate(data.daily.data[0].sunriseTime, data.offset)), '</li>',
        '</ul> </li>',
        '<li> <ul> <li>Sunset:</li>',
        '<li>', format_time(getLocalDate(data.daily.data[0].sunsetTime, data.offset)), '</li>',
        '</ul> </li>',
        '<li> </li>',
        '<li> <ul> <li>Chance of Rain:</li>',
        '<li>', (data.daily.data[0].precipProbability * 100).toFixed
		(2), '%</li>',
        '</ul> </li>',
        '<li><ul><li>Humidity:</li>',
        '<li>', (data.currently.humidity * 100), '%</li>',
        '</ul> </li>',
        '<li></li>',
        '<li><ul> <li>Wind:</li>',
        '<li>',wind_bearing_direction(data.currently.windBearing),' ',(data.currently.windSpeed * 0.621371).toFixed(2), '  mph</li>',
        '</ul></li>',
        '<li> <ul> <li>Feels like:</li>',
        '<li>', format_temp(data.currently.temperature, temp_format.temp_C_F), '˚</li>',
        '</ul> </li>',
        '<li> </li>',
        '<li><ul> <li>Precipitation:</li>',
        '<li>', (data.currently.precipIntensity * 0.0393701).toFixed(2), '</li>',
        '</ul> </li>',
        '<li> <ul><li>Pressure:</li>',
        '<li>', (data.currently.pressure * 0.000295299830714100).toFixed(2), ' in</li>',
        '</ul> </li>',
        '<li> </li>',
        '<li> <ul> <li>Visibility:</li>',
        '<li>', data.currently.visibility, ' mi</li>',
        '</ul> </li>'
    ].join('');
    return li_today_details_list;
}

function getHours_format(utc) {
    var currentHours = utc.getHours();
    var formatter_date;
    var timeOfDay = (currentHours < 12) ? "AM" : "PM";
    currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;
    currentHours = (currentHours == 0) ? 12 : currentHours;
    formatter_date = currentHours + " " + timeOfDay;
    return formatter_date;
}

function getWeekDay(utc) {
    if (utc.getDay() == 1)
        return "Monday";
    else if (utc.getDay() == 2)
        return "Tuesday";
    else if (utc.getDay() == 3)
        return "Wednesday";
    else if (utc.getDay() == 4)
        return "Thursday";
    else if (utc.getDay() == 5)
        return "Friday";
    else if (utc.getDay() == 6)
        return "Saturday";
    else if (utc.getDay() == 0)
        return "Sunday";
}

function check_daynight(data) {
    var sunrise = data.daily.data[0].sunriseTime;
    var sunset = data.daily.data[0].sunsetTime;
    var cloudy_check = data.currently.summary;
    var current_time = data.currently.time;
    if (current_time > sunrise && current_time < sunset && data.currently.cloudCover != 0) {
        return "is-day-cloudy";
    } else if (current_time > sunrise && current_time < sunset && data.currently.cloudCover == 0) {
        return "is-day";
    } else if (current_time < sunrise && current_time > sunset && data.currently.cloudCover != 0) {
        return "is-night-cloudy";
    } else
        return "is-night";
}

function format_temp(temp, format) {
    var temp_formatted;
    temp_formatted = temp;
    if (format == "Celsius") {
        temp_formatted = Math.round((5.0 / 9.0) * (temp - 32.0));
    } else
        temp_formatted = Math.round(temp);
    return temp_formatted;
}

function wind_bearing_direction(windbearing)
{
	var bearings = ["NE", "E", "SE", "S", "SW", "W", "NW", "N"];
		var index = windbearing - 22.5;
		if (index < 0)
			index += 360;
		index = parseInt(index / 45);
		return(bearings[index]);
}